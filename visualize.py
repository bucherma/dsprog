#!/usr/bin/python3
import sys
import matplotlib.pyplot as plt
import numpy as np

def main():
    if len(sys.argv) != 2:
        print("Usage: visualize <input file>")
        return
    inp = sys.argv[1]
    f = open(inp)
    xs = []
    ys = []
    for line in f:
        point = [int(i) for i in line.split()]
        xs.append(point[0])
        ys.append(point[1])

    plt.scatter(xs, ys, marker="s")
    plt.show()

if __name__ == "__main__":
    main()

