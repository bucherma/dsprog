#!/usr/bin/python3
import sys
import matplotlib.pyplot as plt
import numpy as np

def main():
    if len(sys.argv) != 2:
        print("Usage: visualize <output file>")
        return
    inp = sys.argv[1]
    f = open(inp)
    xs = []
    ys = []
    for line in f:
        for p in line.split(";"):
            point = [int(i) for i in p.split()]
            xs.append(point[0])
            ys.append(point[1])
        xs.append(None)
        ys.append(None)

    plt.plot(xs, ys, marker="s")
    plt.show()

if __name__ == "__main__":
    main()

