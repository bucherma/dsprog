all: loesung

WARNINGS = -Wall -Werror
OPTIONAL = -Wextra -Wpedantic -Wshadow
DEBUG = -g -ggdb -fno-omit-frame-pointer
OPTIMIZE = -std=c11

loesung: Makefile loesung.c
	$(CC) -o $@ $(WARNINGS) $(OPTIONAL) $(DEBUG) $(OPTIMIZE) loesung.c
clean:
	rm -f loesung

# Builder uses this target to run your application.
run:
	./loesung < data/example05.dat

